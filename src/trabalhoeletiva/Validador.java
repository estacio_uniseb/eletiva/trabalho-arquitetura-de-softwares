package trabalhoeletiva;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author wtitec
 * @Grupo Willian Ishida | Thiago Gralick
 */
public class Validador implements ValidadorInter {

    private List<String> lista;

    public Validador() {

        this.lista = new ArrayList<String>();
    }

    public List<String> ValidarParametros(Object entidade) {
        Produto produto = (Produto) entidade;

        if (produto.getDescricao().length() < 10) {
            addErro("A descrição do produto, contem menos de 10 caracteres.");
        }
        if ( produto.getValorCusto() >= produto.getValorVenda()) {
            addErro("\nValor de venda e superior ou igual ao de custo");
        }

        long diferencaEmDias = ChronoUnit.DAYS.between(LocalDate.now(), produto.getData());

        if (diferencaEmDias <= 0) {
            addErro("\nData informada para venda e menor ou igual a data de hoje.");
        }

        return lista;
    }

    public void addErro(String erro) {
        lista.add(erro);
    }
}

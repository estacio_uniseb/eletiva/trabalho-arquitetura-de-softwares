package trabalhoeletiva;

import java.time.LocalDate;

/**
 *
 * @author wtitec
 * @Grupo Willian Ishida | Thiago Gralick
 */
public class Produto {

    private String descricao;
    private double valorVenda;
    private double valorCusto;
    private LocalDate data;

    private NovoProduto produtoCriado;

    public Produto RetornaObjetoProduto(NovoProduto produto) {

        Produto produtoNew = new Produto();

        produtoNew.descricao = produto.getDescricao();
        produtoNew.valorVenda = produto.getValorVenda();
        produtoNew.valorCusto = produto.getValorCusto();
        produtoNew.data = produto.getData();

        return produtoNew;

    }

    public String getDescricao() {
        return descricao;
    }

    public double getValorVenda() {
        return valorVenda;
    }

    public double getValorCusto() {
        return valorCusto;
    }

    public LocalDate getData() {
        return data;
    }

    public NovoProduto getProdutoCriado() {
        return produtoCriado;
    }

    public String toString() {
        return "Produto [descricao=" + descricao + ", valorVenda=" + valorVenda + ", valorCusto=" + valorCusto
                + ", data=" + data + ", produtoCriado=" + produtoCriado + "]";
    }

}

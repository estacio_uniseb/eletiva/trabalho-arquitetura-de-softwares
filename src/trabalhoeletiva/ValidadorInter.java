package trabalhoeletiva;

import java.util.List;

/**
 *
 * @author wtitec
 * @Grupo Willian Ishida | Thiago Gralick
 */
public interface ValidadorInter {
    
    public abstract List<String> ValidarParametros(Object o);
    
}


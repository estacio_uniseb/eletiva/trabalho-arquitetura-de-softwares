/**
 * Criar uma aplicação que simule o cadastramento virtual de produtos em um determinado sistema, as informações a serem informadas são: 
 * descrição do produto, valor de venda, valor de custo e data de compra. 
 * 
 * Neste projeto deverá ser aplicado os conceitos de ENTIDADE, OBJETO DE VALOR, VALIDAÇÕES (através do padrão strategy ou specification). 
 * 
 * Regras de negócio: 
 * - Descrição do produto maior que 10 caracteres;
 * - Valor de venda superior ou igual ao valor de custo;
 * - Data de compra inferior ou igual a data do dia;
 * 
 * Regras de implementação (conter):
 * - 1 entidade
 * - 1 objeto de valor
 * - 1 validador
 */
 
package trabalhoeletiva;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

/**
 *
 * @author wtitec
 * @Grupo Willian Ishida | Thiago Gralick
 */

public class TrabalhoEletiva {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {
           
        Validador validar= new Validador();
        Produto produto = new Produto();
        
        /* Descrição do produto maior que 10 caracteres */
        String descricao    = "Descrição do produto";
        
        /* Valor de venda superior ou igual ao valor de custo */
        float  valorVenda   = 12;
        float  valorCusto   = 10;
        
        /* Data de compra inferior ou igual a data do dia */
        LocalDate data = LocalDate.of(2018, Month.NOVEMBER, 1); 
        
        NovoProduto Criado = new NovoProduto(descricao, valorVenda, valorCusto, data);
        Produto ObjetoProduto = produto.RetornaObjetoProduto(Criado);
        List<String> validarParametros = validar.ValidarParametros(ObjetoProduto);

        for (String erros : validarParametros) {
            System.out.println(erros);
        }
    }
    
}

package trabalhoeletiva;

import java.time.LocalDate;

/**
 *
 * @author wtitec
 * @Grupo Willian Ishida | Thiago Gralick
 */

public class NovoProduto {

    private final String descricao;
    private final double valorVenda;
    private final double valorCusto;
    private final LocalDate data;

    public NovoProduto(String descricao, double valorVenda, double valorCusto, LocalDate data) {

        this.descricao = descricao;
        this.valorVenda = valorVenda;
        this.valorCusto = valorCusto;
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public double getValorVenda() {
        return valorVenda;
    }

    public double getValorCusto() {
        return valorCusto;
    }

    public LocalDate getData() {
        return data;
    }
}


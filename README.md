# TRABALHO ARQUITETURA DE SOFTWARES


 Criar uma aplicação que simule o cadastramento virtual de produtos em um determinado sistema, as informações a serem informadas são: 
 descrição do produto, valor de venda, valor de custo e data de compra. 
 
 Neste projeto deverá ser aplicado os conceitos de ENTIDADE, OBJETO DE VALOR, VALIDAÇÕES (através do padrão strategy ou specification). 
 
 Regras de negócio: 
 - Descrição do produto maior que 10 caracteres;
 - Valor de venda superior ou igual ao valor de custo;
 - Data de compra inferior ou igual a data do dia;
 
 Regras de implementação (conter):
 - 1 entidade
 - 1 objeto de valor
 - 1 validador
